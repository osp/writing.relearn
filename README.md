Reprint of the "Relearn" text
======
Revised edition of its original version, published in 2011 in the first issue of the art and criticism review ΔЏ☼ “Pyramid, Tuning Fork, Cogwheel”.

including :

- sla and booklet pdf files of the FR, EN and NL versions of the text
- An odt archive of the late titanpad text
- An HTML conversion of the file mentionned above
- A package of the used fonts to allow quick rework on the sla file

![graph](iceberg/screenshot.png)
